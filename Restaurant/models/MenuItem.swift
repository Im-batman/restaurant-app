//
//  MenuItem.swift
//  Restaurant
//
//  Created by Asar Sunny on 12/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation

struct MenuItem: Codable {
    
    var id: Int
    var name: String
    var detailText: String
    var price: Double
    var category: String
    var imageUrl: URL
    
    enum CodingKeys: String,CodingKey {
        case id
        case name
        case detailText = "description"
        case price
        case category
        case imageUrl
    }

}

struct MenuItems: Codable{
      let items: [MenuItem]
}

