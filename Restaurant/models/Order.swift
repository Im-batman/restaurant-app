//
//  Order.swift
//  Restaurant
//
//  Created by Asar Sunny on 12/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation


struct Order:Codable{
    var menuItems: [MenuItem]

    
    
    init(menuItems:[MenuItem] = []) {
        self.menuItems = menuItems
    }
    
}
