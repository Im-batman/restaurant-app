//
//  IntermediaryModels.swift
//  Restaurant
//
//  Created by Asar Sunny on 12/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation

struct categories: Codable{
    let categories: [String]
}


struct PreparationTime:Codable{
    let prepTime: Int
    
    
    enum CodingKeys: String,CodingKey{
        case prepTime = "preparation_Time"
    }
}
